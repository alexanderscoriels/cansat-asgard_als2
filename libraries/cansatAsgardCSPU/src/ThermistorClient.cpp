/*
   ThermistorClient.cpp
*/

#include "ThermistorClient.h"

ThermistorClient::ThermistorClient(	Thermistor *therm1,
		Thermistor *therm2,
		Thermistor *therm3 ):
  thermistor1(therm1)
#ifdef INCLUDE_THERMISTOR2
, thermistor2(NULL)
#endif
#ifdef INCLUDE_THERMISTOR3
, thermistor3(NULL)
#endif
{
}

void ThermistorClient::setThermistors(	Thermistor *therm1,
		Thermistor *therm2,
		Thermistor *therm3 )
{
  thermistor1=therm1;
#ifdef INCLUDE_THERMISTOR2
  thermistor2=therm2;
#endif
#ifdef INCLUDE_THERMISTOR3
  thermistor3=therm3;
#endif
}

bool ThermistorClient::readData(CansatRecord& record) const {
  if (thermistor1) {
	  record.temperatureThermistor1 = thermistor1->readTemperature();
  }
#ifdef INCLUDE_THERMISTOR2
  if (thermistor2) {
	  record.temperatureThermistor2 = thermistor2->readTemperature();
  }
#endif
#ifdef INCLUDE_THERMISTOR3
  if (thermistor3) {
	  record.temperatureThermistor3 = thermistor3->readTemperature();
  }
#endif
  return true;
}

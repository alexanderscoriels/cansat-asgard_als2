/*
    FlashLogger.h
*/
#pragma once

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS

#include "Logger.h"
#include <Adafruit_SPIFlashPatched.h>
#include <Adafruit_SPIFlash_FatFsPatched.h>

/** @ingroup cansatAsgardCSPU
    @brief  A class providing the SPI flash storage services to the StorageManager on a Feather M0 Express board.

   This class is only relevant fro the Feather M0 Express board, which has
   on-board flash memory on a dedicated SPI bus.
   Initialization of library: this initializes the SPI Flash library transparently.

   @par Design options:
   1. Do we close the file after each write or just flush it ? Performance is excellent with both options (<30 msec / write, for about 60 bytes à data).
   2. Would it be better to flush or close/reopen in doIdle() rather than in log?  Implemented but no significant improvement.
*/
class FlashLogger : public Logger {
  public:
    /** Important note: only one instance of the FlashLogger is allowed. If more than one is created (which is a very bad idea, the
        underlying fatfs object allocated large buffers.
    */
    FlashLogger();
    virtual ~FlashLogger();
    /** Append data to the logfile, possibly completed with a "\n" and flush the file
      @ param addFinalCR If true a final end-of-line character is added.
    */
                       
    bool log(const String& data, const bool addFinalCR = true);

    /**  @return the size in bytes of the logfile in bytes (does not work for files larger than 2Gb on
        8-bits boards due to unsigned long limitation).
    */
    virtual unsigned long fileSize() ;
  protected:
    /** Initialize the underlying storage structures
        @return True if everything ok, false otherwise.
    */
    bool initStorage();
    /** Return the free space on storage media. */
    virtual float getFreeSpaceInMBytes();
    virtual bool fileExists(const char* name) {
      return fatfs.exists(name);
    }
  private:
    Adafruit_SPIFlash flash;
    Adafruit_W25Q16BV_FatFs fatfs;
    static bool storageInitialized; /**< true if the storage lib was initialized by any instance */ 
    bool instanceInitialized;  /**< true if this instance is initialized. */
};

#endif // board type

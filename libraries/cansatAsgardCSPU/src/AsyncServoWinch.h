#pragma once
#include "ServoWinch.h"
#include <elapsedMillis.h>
#include <Arduino.h>

/** @ingroup cansatAsgardCSPU
    @brief Subclass of ServoWinch that enables control over motor speed and asynchronous operation to prevent blocking the can during the time required to obtain the desired position.
*/

class AsyncServoWinch : public ServoWinch {
  public:
    using ServoWinch::ServoWinch;

    /** Start the servo-motor and set it to the initial length. Call this before calls to any other method.
      @param PWM_Pin The PWM pin used by the motor
      @param initPositionType the type of the initial position: angle (in degree; 0 is fully retracted, 180 is fully extended) or ropeLength (in mm)
      @param initPosition the desired initial position. In degrees if initPositionType is angle, in mm if initPositionType is ropeLength
      @param mmPerMoveToUse When moving servo motor, each updateFrequency milliseconds, rope will be extended/retracted by this amount of mm

      @return true if servo was successfully initialized and set to initPosition, false otherwise
    */
    bool begin(byte PWM_Pin, ValueType initPositionType, uint16_t initPosition, uint8_t mmPerMoveToUse);

    /** Optionally change the position and power off the motor. If you want to change the position, the motor will be stopped by the run() when it is in the correct position. If you want to use the motor later, you will have to call begin again.
      @warning The motor may not be exactly where you expect it to be when powering off. This is due to the fact that when the last move is done (before reaching the target end position, the motor will almost immediately power off (and may not completely reach the destination before powering off). This is negligable in the majority of the cases.
      @param moveBeforeEnding Should the motor move to the endPosition before powering off.
      @param endPositionType The type of the parameter endPosition
      @param endPosition The position where the motor should go to before powering off (if moveBeforeEnding is true)

      @return true if the servo target was successfully set to the ending position, if requested, or if it was successfully powered off. false otherwise
    */
    bool end(bool moveBeforeEnding = false, ValueType endPositionType = ValueType::ropeLength, uint16_t endPosition = 0);

    /** Set the servo-motor to the desired angle or set the change the rope length to the desired value. The motor will rotate using the motorSpeedToUse specified in the begin call.
      @param type the type of the value: angle (in degree; 0 is fully retracted, 180 is fully extended) or ropeLength (in mm)
      @param value The desired value to be set. In degrees if type is angle, in mm if type is ropeLength

      @return true if the target was successfully set, false otherwise
    */
    virtual bool setTarget(ValueType type, uint16_t value);

    /** Method to be constantly called. It will make the servo move.
    */
    void run();

    /** Set by how much should the motor overshoot the target
      @param overshootTargetByToUse By how many mm should the target be overshot. In other words, when this is set to 10 mm for example, the motor will move the rope 10 mm more than needed and then go back to the wanted position. However, the rope length will NEVER go of bounds (less than 0 or greater than maxRopeLen): so even when this variable would mean that the rope length should go out of bounds, it won't.
    */
    void setOvershootBy(uint16_t overshootTargetByToUse);

    /** Get the target rope length
      @return the target rope length
    */
    uint16_t getTargetRopeLen() const;

    /** Get the target pulse width
      @return the target pulse width
    */
    uint16_t getTargetPulseWidth() const;

    /** Check if the target has been reached
      @return true if the target has been reached, false otherwise
    */
    bool isAtTarget() const;
	
	/** Check the time elapsed since target was reached.
	  @return milliseconds elapsed since target was reached. (if target is not reached, returns 0)
	*/
	unsigned long elapsedSinceTargetReached();

    static constexpr uint16_t updateFrequency = 500;		/**< When moving servo motor, each updateFrequency milliseconds, rope will be extended/retracted by mmPerMove millimeters */


  protected:
    uint16_t targetRopeLen;				/**< The final rope length target */
    uint16_t targetPulseWidth;			/**< The final pulse width target */

  private:
    elapsedMillis elapsedSinceLastMove;	/**< The elapsed milliseconds since the servo was last moved by the run() */
	unsigned long tsTargetReached = 0;	/**< When was the target reached. (value unreliable if target not reached) */
    uint8_t mmPerMove;					/**< How much will the servo move every updateFrequency milliseconds */
    bool ending = false;				/**< True if the servo motor is currently being moved to its final position, false otherwise. When it's true, no other setTarget will be accepted. */
    int16_t overshootTargetBy = 0;		/**< By how much will the target be overshot (including direction) (mm). It is updated by the setTarget method */
    uint16_t inputOvershootTargetBy = 0;/**< The raw value supplied by the user for the overshooting of targets. (mm) */
    friend class ServoWinch_Test;		/**< Friend class used for testing */
};

/*
   ThermistorNTCLG100E2104JB.h
*/

#pragma  once
#include "ThermistorSteinhartHart.h"

/**  @ingroup cansatAsgardCSPU
   @brief This a subclass of ThermistorSteinhartHart customized for thermistor NTCLG100E2104JB, NTCSMELFE3203Jx  or  NTCLG100E2203Jx.
   Use this class to read thermistor resistance and convert to degrees.
   Wiring: VCC to thermistor, thermistor to serialresistor, serialresistor to ground.

*/
class ThermistorNTCLG100E2104JB: public ThermistorSteinhartHart {
  public:
  /**
   *  @param theVcc  The voltage supplied to the serial resistor+thermistor assembly
   *  @param theAnalogPinNbr this is the pin of the card in which we put the cable to read the resistance.
   *  @param theSerialResistor the serial resistor value, in ohms.
   */
    ThermistorNTCLG100E2104JB(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorSteinhartHart(theVcc, theAnalogPinNbr, 100000.0, 0.003354016,  0.0002569850, 2.620131E-06, 6.383091E-08, theSerialResistor) {};
    virtual ~ThermistorNTCLG100E2104JB(){};

};

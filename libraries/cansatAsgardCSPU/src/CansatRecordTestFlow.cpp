/*
 * CansatRecordTestFlow.cpp
 *
 *  Created on: 31 Jan 2021
 *      Author: Alain
 */

#include "CansatRecordTestFlow.h"
#define DEBUG
#include "DebugCSPU.h"


#define DBG_DIAGNOSTIC 1
#define DBG_PARSING 0

CansatRecordTestFlow::CansatRecordTestFlow(uint16_t theBufferSize):
	mySD(), buffer(nullptr), bufferSize(theBufferSize), currentLine(0) {
}

CansatRecordTestFlow::~CansatRecordTestFlow() {
	closeInputFile();
	if (buffer) {
		free(buffer);
	}
}

void CansatRecordTestFlow::closeInputFile() {
	if (dataFile) {
		dataFile.close();
	}
}

bool CansatRecordTestFlow::openInputFile(const char* fileName,
		const byte SD_ChipSelect) {
	if (!buffer)
	{
		  bool result = mySD.begin(SD_ChipSelect);
		  if (!result) {
		    DPRINTS(DBG_DIAGNOSTIC, "error in mySD.begin CS=");
		    DPRINTLN(DBG_DIAGNOSTIC, SD_ChipSelect);
		    return false;
		  }

		  buffer = (char*) malloc(bufferSize);
		  if (!buffer) {
			  DPRINTS(DBG_DIAGNOSTIC, "Could not allocated internal buffer, size=");
			  DPRINTLN(DBG_DIAGNOSTIC, bufferSize);
			  return false;
		  }
	}

    closeInputFile(); // In case this is not the first file read.

	bool result = false;
	Serial << "Reading record from " << fileName << ENDL;
	dataFile = mySD.open(fileName, FILE_READ);

	if (dataFile) {
		result = true;
		currentLine=0;
	}
	else {
		DPRINTSLN(DBG_DIAGNOSTIC, "error opening file");
	}
	return result;
}

bool CansatRecordTestFlow::getRecord(CansatRecord& record) {
	DASSERT(dataFile);
	DASSERT(buffer);
	if (record.getMaxCSV_Size() > bufferSize) {
		Serial << "Error: max CSV size of record is "<< record.getMaxCSV_Size()
			   << " while buffer size is " << bufferSize << " characters" << ENDL;
		Serial << "Increase value of buffer size in when calling constructor and try again" << ENDL;
		delay(500);
		exit(-1);
	}
	uint16_t read=0;
	bool readRecord=false;
	while (!readRecord && dataFile.available()) {
		// Read until we find a non-empty line, not starting with '#'
		do {
			read=dataFile.fgets(buffer, bufferSize);
			currentLine++;
		} while (dataFile.available() && (read ==0 || (read >0 && buffer[0]=='#')));

		if (read >0) {
			// Something was read which is not a comment.
			if (read==bufferSize-1) {
				// We completely filled the buffer
				if (buffer[bufferSize-2] != '\n') {
					// We could not read a complete line
					Serial << "Error: line " << currentLine << " could not fit in the line buffer:" << ENDL;
					Serial << " '" << buffer << "... etc." << ENDL;
					Serial << "Increase buffer size or limit line length to " << bufferSize-1 << " characters maximum" << ENDL;
					return false;
				}
			}
			readRecord=parseBufferIntoRecord(record);
		}
		else if (read==0) {
			Serial << "End of input file reached" << ENDL;
			closeInputFile();
			return false;
		} else {
			// read error.
			Serial << "Error while reading file" << ENDL;
			return false;
		}
		if (!readRecord) {
			Serial << "Error while parsing line " << currentLine << " into record." << ENDL;
			Serial << "  Current content of record:" << ENDL;
			Serial << "  ";
			record.printCSV(Serial, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Content);
			Serial << ENDL;
			Serial << "  Line ignored." << ENDL;
		}
	} // while (!readRecord).
	return readRecord;
}

bool CansatRecordTestFlow::parseBufferIntoRecord(CansatRecord& record) {
	record.clear();
	DPRINTS(DBG_PARSING, "Parsing buffer '");
	DPRINTS(DBG_PARSING, buffer)
	DPRINTSLN(DBG_PARSING, "'")

	char* ptr=strtok(buffer,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.timestamp=atoi(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.newGPS_Measures=(atoi(ptr)==1);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.GPS_LatitudeDegrees=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.GPS_LongitudeDegrees=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.GPS_Altitude=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_GPS_VELOCITY
	record.GPS_VelocityKnots=atof(ptr);
#endif

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_GPS_VELOCITY
	record.GPS_VelocityAngleDegrees=atof(ptr);
#endif

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.temperatureBMP=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.pressure=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.altitude=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_REFERENCE_ALTITUDE
	record.refAltitude=atof(ptr);
#endif

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_DESCENT_VELOCITY
	record.descentVelocity=atof(ptr);
#endif

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
	record.temperatureThermistor1=atof(ptr);

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_THERMISTOR2
	record.temperatureThermistor2=atof(ptr);
#endif

	ptr=strtok(nullptr,",");
	DPRINTSLN(DBG_PARSING, ptr);
	if (!ptr) return false;
#ifdef INCLUDE_THERMISTOR3
	record.temperatureThermistor3=atof(ptr);
#endif

	return populateSecondaryMissionData(record, ptr);
}

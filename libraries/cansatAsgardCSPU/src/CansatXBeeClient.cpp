/*
 CansatXBeeClient.cpp
 */

#include "CansatXBeeClient.h" // This one must be outside the #ifdef to have the symbol defined

#ifdef RF_ACTIVATE_API_MODE

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_STREAM 0

bool CansatXBeeClient::send(const CansatRecord& record,
		int timeOutCheckResponse) {
	// Beware of byte alignment. Only even addresses can be accessed directly
	// in SAMD 16-bit architecture.
	uint8_t recordSize= (uint8_t) record.getBinarySize();
	uint8_t bufferSize=recordSize + 2; // Do not use sizeof !
	uint8_t buffer[bufferSize];
	buffer[0] = (uint8_t) CansatFrameType::DataRecord;
	buffer[1] = 0;
	if (!record.writeBinary(buffer + 2,recordSize)) {
		DPRINTS(DBG_DIAGNOSTIC, "Error streaming record");
		return false;
	}
	if (DisplayOutgoingMsgOnSerial) {
		Serial << ENDL << "  --- Outgoing record ---" << ENDL;
		record.print(Serial);
		Serial << ENDL << "  --- End of record ---" << ENDL;
		displayFrame(buffer, bufferSize);
	}
	return XBeeClient::send((uint8_t *) buffer, bufferSize, timeOutCheckResponse);
}

bool CansatXBeeClient::receive(char* string, CansatFrameType &stringType, uint8_t& seqNbr) {
	uint8_t *buffer;
	uint8_t payloadSize;
	bool result=false;
	if (XBeeClient::receive(buffer, payloadSize)) {
		if (getString(string, stringType, seqNbr, buffer, payloadSize, true)) {
			if (DisplayIncomingMsgOnSerial) {
				Serial << ENDL << "  --- Incoming string ---" << ENDL;
				displayString(buffer, payloadSize);
			}
			result=true;
		} // received string
	} // received anything
	return result;
}

bool CansatXBeeClient::receive(CansatRecord& record, char* string,
		CansatFrameType &stringType, uint8_t& stringSeqNbr, bool& gotRecord) {
	uint8_t *buffer;
	uint8_t payloadSize;
	bool result = false;
	if (XBeeClient::receive(buffer, payloadSize)) {
		gotRecord=(buffer[0] == (uint8_t) CansatFrameType::DataRecord);
		if (gotRecord) {
			result = getDataRecord(record, buffer, payloadSize);
		} else {
			result = getString(string, stringType, stringSeqNbr,  buffer, payloadSize);
		}

		if (DisplayIncomingMsgOnSerial) {
			if (gotRecord) {
				Serial << ENDL << "  --- Incoming record ---" << ENDL;
				record.print(Serial);
				Serial << ENDL << "  --- End of record ---" << ENDL;
			} else {
				Serial << ENDL << "  --- Incoming string ---" << ENDL;
				displayString(buffer, payloadSize);
			}
		}
	}
	return result;
}

void CansatXBeeClient::openStringMessage(CansatFrameType frameType, uint8_t seqNbr) {
	DPRINTS(DBG_STREAM, "CansatXBeeClient::openStringMessage(), type= ");
	DPRINTLN(DBG_STREAM, (uint8_t ) frameType);
	XBeeClient::openStringMessage((uint8_t) frameType, seqNbr);
}

bool CansatXBeeClient::getDataRecord(CansatRecord& record, const uint8_t* data,
		uint8_t dataSize) const {
	uint8_t recordSize=record.getBinarySize();
	if (dataSize != (recordSize + 2)) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
		DPRINT(DBG_DIAGNOSTIC, dataSize);
		DPRINTS(DBG_DIAGNOSTIC, ", CansatRecord size=");
		DPRINTLN(DBG_DIAGNOSTIC, recordSize);
		return false;
	}
	if (data[0] != (uint8_t) CansatFrameType::DataRecord) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
		DPRINT(DBG_DIAGNOSTIC, data[0]);
		DPRINTS(DBG_DIAGNOSTIC, ", expected ");
		DPRINTLN(DBG_DIAGNOSTIC, (uint8_t ) CansatFrameType::DataRecord);
		return false;
	}

	if (!record.readBinary(data+2, dataSize-2)) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error reading record");
		return false;
	}
	return true;
}

bool CansatXBeeClient::getString(char* str, CansatFrameType &stringType,
		uint8_t& seqNbr,const uint8_t* data, uint8_t dataSize,
		bool silentIfError) const {
	uint8_t sType;
	bool result = XBeeClient::getString(str, sType, seqNbr, data, dataSize);

	stringType = (CansatFrameType) data[0];
	switch (stringType) {
	case CansatFrameType::StatusMsg:
	case CansatFrameType::CmdRequest:
	case CansatFrameType::CmdResponse:
	case CansatFrameType::StringPart:
		break;
	default:
		if (!silentIfError) {
			DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
			DPRINT(DBG_DIAGNOSTIC, data[0]);
			DPRINTS(DBG_DIAGNOSTIC,
					", expected StatusMsg, CmdRequest, CmdResponse or StringPart).");
		}
		result = false;
		break;
	} // switch
	return result;
}
#endif

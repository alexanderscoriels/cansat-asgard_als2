/*
   Test for class BMP_Client.
   - Use constat csvFormat to select output format.
   - When not in CSV format, the range of pressure and altitude is displayed: this
     allow for testing the effect of the number of samples taken when reading pressure.
   - First 4 seconds of operation are ignored to ensure stability (averaging buffers have to fill).
   - WARNING: for performance on calculation of descent velocity to be relevant, the acquisition
              period must be the operational one (about 70 msec, see CansatConfig.h) and all
              debugging must be off.


   Wiring:
      µC		BMP280
      3V		Vin
      GND		GND
      SCL		SCK (with pull-up resistor to Vin)
 	  SDA		SDI (with pull-up resistor to Vin)
*/

#include "elapsedMillis.h"
#include "BMP_Client.h"

CansatRecord record;
constexpr float mySeaLevelPressure = 1011.0;
constexpr bool  csvFormat = false;
constexpr uint32_t testPeriod = CansatAcquisitionPeriod; // set to CansaTAcquisitionPeriod for realistic test, or to larger value for debugging.
constexpr uint32_t ResetPeriod=5000;
constexpr uint32_t IgnoredSamples =
	(BMP_NumPeriodsForDescentVelocityCalculation+BMP_NumDescentVelocitySamplesToAverage);
	// number of samples ignored by test program at startup to ensure stability.

BMP_Client bmp;
float 	maxPressure = 0,
		minPressure = 100000,
		maxAltitude = -1000,
		minAltitude = 100000,
		minVelocity = 10000,
		maxVelocity = -10000;
elapsedMillis elapsed, elapsedForReset;
uint32_t counter=0;

void resetRanges() {
	maxPressure = 0;
	minPressure = 100000;
	maxAltitude = -1000;
	minAltitude = 100000;
	minVelocity = 10000;
	maxVelocity = -10000;
}

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  Wire.begin();
  Serial << ENDL << ENDL;
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial << "Could not find a valid BMP sensor, check sensor time and wiring!" << ENDL;
    Serial << "Expected sensor: Adafruit ";
#ifdef BMP_USE_BMP3XX_MODEL
    Serial << "BMP3xx" << ENDL;
#else
    Serial << "BMP280" << ENDL;
#endif
    Serial << "Model selection is controlled by symbol BMP_USE_BMP3XX_MODE in CansatConfig.h" << ENDL;
    exit(-1);
  }
  elapsed = 0;
  elapsedForReset=0;
  Serial << "Running test with a period of " << testPeriod << " msec." << ENDL;
  Serial << "Ignoring the first " << IgnoredSamples << " samples to allow filter initialization" << ENDL;
  Serial << "Acquisition period: " << testPeriod << " msec" << ENDL;
  Serial << "BMP_NumPeriodsForDescentVelocityCalculation : " << BMP_NumPeriodsForDescentVelocityCalculation << ENDL;
  Serial << "BMP_NumDescentVelocitySamplesToAverage : " <<BMP_NumDescentVelocitySamplesToAverage << ENDL;
}

void loop() {
  if (elapsed < testPeriod) {
    return;
  }

  counter++;
  elapsed = 0;
  record.timestamp = millis();
  uint32_t before = millis();
  bool result = bmp.readData(record);
  uint32_t duration = millis() - before;

  // Ignore first samples to ensure stability
  if (counter <= IgnoredSamples) {
    return;
  }
  if (result == false) {
    Serial << "*** Error reading data***" << ENDL;
    return;
  }

  // A. Record extreme values
  if (record.pressure > maxPressure) maxPressure = record.pressure;
  if (record.pressure < minPressure) minPressure = record.pressure;
  if (record.altitude > maxAltitude) maxAltitude = record.altitude;
  if (record.altitude < minAltitude) minAltitude = record.altitude;
#ifdef INCLUDE_DESCENT_VELOCITY
  if (record.descentVelocity > maxVelocity) maxVelocity = record.descentVelocity;
  if (record.descentVelocity < minVelocity) minVelocity = record.descentVelocity;
#endif

  // B. Print results
  if (csvFormat) {
    Serial << record.timestamp << "," << duration << "," << record.temperatureBMP << "," << record.pressure << "," << record.altitude;
#ifdef INCLUDE_DESCENT_VELOCITY
    Serial << "," << record.descentVelocity;
#endif
    Serial << ENDL;

  } else {
    Serial << record.timestamp << ", dur.=" << duration << "msec, temp: " << record.temperatureBMP << "°C, pressure:" << record.pressure << " hPa, " << "altitude:" << record.altitude << " m";
#ifdef INCLUDE_DESCENT_VELOCITY
    Serial << ", desc. velocity=" << record.descentVelocity;
#endif
    Serial << "- ranges: press=[" << minPressure << ";" << maxPressure << "] (" << maxPressure - minPressure << "), altitude=["
           << minAltitude << ";" << maxAltitude << "] (" << maxAltitude - minAltitude << ")";
#ifdef INCLUDE_DESCENT_VELOCITY
    Serial << ", veloc.=[" <<  minVelocity  << ";" << maxVelocity << "] (" << maxVelocity - minVelocity << ")";
#endif
    Serial <<  ENDL;
  }

  if (elapsedForReset>=ResetPeriod) {
	  Serial << ENDL << " *** Velocity range: " << maxVelocity - minVelocity << "m/s" << ENDL << ENDL;
	  resetRanges();
	  elapsedForReset=0;
  }
} // loop

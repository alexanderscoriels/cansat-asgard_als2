#include "TorusRecord.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG_BINARY 0

void TorusRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << parachuteRopeLen << separator;
  str << parachuteTargetRopeLen << separator;
  str << (int) controllerInfo << separator;
  str << flightPhase;
  if (finalSeparator) str << separator;
}

void TorusRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "parachuteRopeLen,parachuteTargetRopeLen,ctrlCode,phase";
  if (finalSeparator) str << separator;
}

void TorusRecord::clearSecondaryMissionData() {
  parachuteRopeLen = 0;
  parachuteTargetRopeLen = 0;
  controllerInfo=TorusControlCode::NoData;
  flightPhase=0;
}

uint16_t TorusRecord::getSecondaryMissionMaxCSV_Size() const {
  return 1 /*,*/ + 5 /*parachuteRopeLen (uint16_t)*/ + 1 /*,*/ + 5 /*parachuteTargetRopeLen (uint16_t)*/ + 1 /*,*/
		  + 1 /*,*/ + 3 /* ctrlCode */ + 1 /*,*/ + 3 /* flightPhase */;
}

uint16_t TorusRecord::getSecondaryMissionCSV_HeaderSize() const {
  return 1 /*,*/ + 39 /*parachuteRopeLen,parachuteTargetRopeLen*/ + 15; /*,ctrlCode,phase*/
}

void TorusRecord::printSecondaryMissionData(Stream& str) const {
  str << "parachuteRopeLen: " << parachuteRopeLen << ENDL;
  str << "parachuteTargetRopeLen: " << parachuteTargetRopeLen << ENDL;
  str << "controller info: " << (int) controllerInfo << ENDL;
  str << "Flight phase: " << flightPhase << ENDL;
}

uint8_t TorusRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  written += writeBinary(dst, remaining, parachuteRopeLen);
  written += writeBinary(dst, remaining, parachuteTargetRopeLen);
  uint8_t ctrlInfo=(((int)controllerInfo) << 4) + (flightPhase % 0xF);
  DPRINTS(DBG_BINARY, " writeBinary: flightPhase=");
  DPRINTLN(DBG_BINARY, flightPhase);
  DPRINTS(DBG_BINARY, " writeBinary: controllerInfo=");
  DPRINTLN(DBG_BINARY, (int)controllerInfo);
  DPRINTS(DBG_BINARY, " writeBinary: ctrlInfo=");
  DPRINTLN(DBG_BINARY, ctrlInfo, HEX);

  written += writeBinary(dst, remaining, ctrlInfo);


  return written;
}

uint8_t TorusRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  uint8_t read = 0;
  uint8_t ctrlInfo=0;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  read += readBinary(src, remaining, parachuteRopeLen);
  read += readBinary(src, remaining, parachuteTargetRopeLen);

  read += readBinary(src, remaining, ctrlInfo);
  DPRINTS(DBG_BINARY, " readBinary: ctrlInfo=");
  DPRINTLN(DBG_BINARY, ctrlInfo, HEX);
  flightPhase=ctrlInfo & 0xF; // extract LSB
  DPRINTS(DBG_BINARY, " readBinary: flightPhase=");
  DPRINTLN(DBG_BINARY, flightPhase);
  DPRINTS(DBG_BINARY, " readBinary: controllerInfo=");
  DPRINTLN(DBG_BINARY, (ctrlInfo >> 4));

  controllerInfo=(TorusControlCode) (ctrlInfo >> 4);

  return read;
}

uint8_t TorusRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(parachuteRopeLen) + sizeof(parachuteTargetRopeLen)
		  + sizeof(uint8_t);  // controllerInfo + flight phase = 1 byte.
}

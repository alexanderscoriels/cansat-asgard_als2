/*
   Utility functions for testing the EEPROM_Bank
*/

#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "utilityFunctions.h"

//#define DUMP_TEST_DATA   // Define to dump extensively the test records written to and read from EEPROM.

byte writeDataCounter = 0; // A number to discriminate between successive data records.
byte readDataCounter = 0;
TestRecord testData;

void printSize(unsigned long size) {
  Serial << size << F(" bytes (") << size / 1024.0  << F("k)");
}

void dumpTestData(TestRecord r)
{
  Serial << F("   aByte : ") << r.aByte << ENDL;
  Serial << F("   anInt : ") << r.anInt << ENDL;
  Serial << F("   aFloat: ") << r.aFloat << ENDL;
  Serial << F("   anUInt: ") << r.anUnsignedInt << ENDL;
  Serial << F("   aChar : ") << (int) r.aChar << ENDL;
}

unsigned long getExpectedTotalSize(HardwareScanner &hw) {
  unsigned long size = 0;
  for (int i = 0; i < hw.getNumExternalEEPROM(); i++) {
    if (hw.isI2C_SlaveUpAndRunning(hw.getExternalEEPROM_I2C_Address(i))) {
      size += ((long) hw.getExternalEEPROM_LastAddress(i) + 1);
    } // if
  } //for
  return size;
}

void checkSpace(const EEPROM_Bank &bank, unsigned long expectedSize, unsigned long expectedFreeSpace) {
  unsigned long totalSize = bank.getTotalSize();
  unsigned long freeSpace = bank.getFreeSpace();
  Serial << F("Total/Free size: ");
  printSize(totalSize);
  Serial << F("/");
  printSize(freeSpace);
  Serial << F(". Expected: ")  << expectedSize << F("/")
         << expectedFreeSpace << ENDL;
  DASSERT(expectedFreeSpace == freeSpace);
  DASSERT(expectedSize == totalSize);
}

void doIdle(EEPROM_Bank &bank, byte durationInSeconds)
{
  elapsedMillis em = 0;
  Serial    << F("Checking delayed header update... (1 dot = 500 msec):")
            << ENDL << F("   ");
  while (em < durationInSeconds * 1000)
  {
    bank.doIdle();
    Serial << F(".");
    Serial.flush();
    delay(500);
  }
  Serial << ENDL;
}


void storeDataRecords(EEPROM_Bank * bank, int numRecords) {
  Serial << "Storing " << numRecords << " records..." << ENDL;
  for (int i = 0; i < numRecords; i++) {
    testData.aByte = (writeDataCounter);
    bool written = bank->storeOneRecord((byte*) (&testData), sizeof(testData));
    DASSERT(written == true);
#ifdef DUMP_TEST_DATA
    dumpTestData(testData);
#else
    Serial << "   Wrote 0x";
    Serial.println(testData.aByte, HEX);
#endif

    writeDataCounter--;
  }
}


void readDataRecords(EEPROM_BankWithTools* bank, int numRecords) {
  Serial << "Reading & checking " << numRecords << " records..." << ENDL;
  for (int i = 0; i < numRecords; i++) {
    int read = bank->readData((byte*) (&testData), sizeof(testData));
    DASSERT(read == sizeof(testData));
#ifdef DUMP_TEST_DATA
    dumpTestData(testData);
#else
    Serial << "   Read 0x";
    Serial.print(testData.aByte, HEX);
#endif
    Serial << ", expected 0x" ;
    Serial.println(readDataCounter, HEX);
    DASSERT(testData.aByte == readDataCounter);
    DASSERT(testData.aChar == 11);
    DASSERT(testData.aFloat == 3.141592);
    DASSERT(testData.anInt == -12);
    DASSERT(testData.anUnsignedInt == 0x9876);
    readDataCounter--;
  }
}

void initTestData(byte readCounterValue, byte writeCounterValue )
{
  writeDataCounter = writeCounterValue;
  readDataCounter = readCounterValue;
  testData.aByte = writeCounterValue;
  testData.aChar = 11;
  testData.aFloat = 3.141592;
  testData.anInt = -12;
  testData.anUnsignedInt = 0x9876;
}



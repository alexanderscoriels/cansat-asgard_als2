/*
 * EEPROM_CSPU.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup EEPROM_CSPU 
 * in the class documentation block.
 */

 /** @defgroup EEPROM_CSPU EEPROM_CSPU library 
 *  @brief A couple of classes to access an external EEPROM and manage several EEPROMs as one logical
 *  storage.
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - elapsedMillis
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS) and further enriched
 *  during the next projects. 
 */


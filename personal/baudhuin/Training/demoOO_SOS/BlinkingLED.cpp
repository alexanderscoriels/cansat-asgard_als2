
#include "BlinkingLED.h"


BlinkingLED::BlinkingLED(byte thePinNumber, unsigned long theDuration) {
   duration=theDuration;
   pinNumber=thePinNumber;
   pinMode(pinNumber, OUTPUT);
   ts=millis();
}

void BlinkingLED::run() {
    if ((millis() - ts) >= duration) {
        digitalWrite(pinNumber, !digitalRead(pinNumber));
        ts=millis(); //set timestamp to now.
    }
}

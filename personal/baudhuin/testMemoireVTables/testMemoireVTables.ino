/*
    Test how costly a real OO design can be
*/

#include "config.h"

#include "myClasses.h"

DataManager mgr;
EEPROM_DataManager EEmgr;

void setup() {
  // put your setup code here, to run once:
  DINIT(9600);
  mgr.ProcessData(NULL);
  EEmgr.ProcessData(NULL);
}

void loop() {
  // put your main code here, to run repeatedly:

}

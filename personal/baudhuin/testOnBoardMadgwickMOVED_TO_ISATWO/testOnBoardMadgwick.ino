/*
   Run the IMU Calibration and Madgwick quaternion update on-board, as fast as possible.
   Tests results on FeatherBoard: period=4.48 msec with just the IMU reading/calibration/fusion.
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IMU_FusionFilter.h"

constexpr unsigned long UpdateDelay = 5;   // msec.
constexpr unsigned long TransferDelay = 100; // msec
//define DEBUG_TIMING     // Define to see the timing (pollutes the output for the RT-GUI...
#define OUTPUT_TO_RT_GUI // Define to output complete ground records for the RT-GUI.

IMU_FusionFilter filter;
elapsedMillis elapsed, elapsedSinceTransfer;
unsigned long counter = 0;

void setup() {
  DINIT(115200);
  if (!filter.begin(UpdateDelay)) {
    Serial << "Error during IMU_FusionFilter init" << ENDL;
  }
  elapsed = elapsedSinceTransfer = 0;
  Serial << "Setup done" << ENDL << ENDL;
}

void loop() {
  if (elapsed >= UpdateDelay) {
    elapsed = 0;
    filter.update();
    counter++;
  }
  if (elapsedSinceTransfer >= TransferDelay) {
    unsigned long ts = elapsedSinceTransfer;
#ifdef DEBUG_TIMING
    Serial << "Updated " << counter << " times in " << ts << " msec. Period = " << ((float) ts) / counter << " msec." << ENDL;
#endif
    elapsedSinceTransfer = 0;
    counter = 0;
#ifdef OUTPUT_TO_RT_GUI
    // Output as IsaTwoGroundRecord for feeding the RT-GUI.
    Serial << "100," << millis() << ',';
    for (int i = 0; i < 18 ; i++ ) Serial << "0,"; // Complete IsaTwoRecord.
    for (int i = 0; i < 12 ; i++ ) Serial << "0,"; // Start GroundRecord
    Serial << filter.getRoll() << ",";
    Serial << filter.getYaw() << ",";
    Serial << filter.getPitch() << ",";
    for (int i = 0; i < 24 ; i++ ) Serial << "0,"; // Terminate GroundRecord
    Serial << ENDL;
#endif
  }
}

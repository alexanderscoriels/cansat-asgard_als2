#pragma once
#include "Arduino.h"


class BlinkingLED {                                           //Declares the class BlinkkingLED
  public:                                                     //Acces specifier for public members
          
    BlinkingLED(byte pinNumber, uint16_t duration);           //Declares the constructor for the BlinkingLED class with parameters for the pin number and the "frequency" of the blinks

    void run();                                               //Declares the run() method

  private:                                                    //Acces specifier for private members 
    byte pinNumber;                                           //Private variable for the pin number the blinking will be ran on
    uint16_t duration;                                        //Private variable for the intervals the blinking will be ran on
    unsigned long ts;                                         //Private variable for a time stamp

} ;

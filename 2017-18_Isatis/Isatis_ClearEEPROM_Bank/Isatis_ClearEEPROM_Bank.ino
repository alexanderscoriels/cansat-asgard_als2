/*
    This sketch just clears the EEPROM_Bank assuming it does not contain anything else than Isatis records.
*/

#include "EEPROM_Bank.h"
#include "DebugCSPU.h"
#include "IsatisHW_Scanner.h"
#include "IsatisConfig.h"
#include "IsatisDataRecord.h"

void setup() {
  Serial.begin(19200);
  while (!Serial) ;

  IsatisHW_Scanner hw;
  hw.init(1, 127);
  hw.printFullDiagnostic(Serial);

  EEPROM_Bank eeprom(5);
  bool result = eeprom.init(EEPROM_KeyValue, hw, sizeof(IsatisDataRecord));
  if (!result) {
    Serial << F("Error during EEPROM_Bank init") << ENDL;
    Serial << F("Inconsistency between expected and actual record size can be caused by a change in the ") << ENDL;
    Serial << F("configuration of the data record. Did you modify USE_MINIMAL_DATARECORD in IsatisConfig?") << ENDL;
    Serial << F("In this case, just change the EEPROM_Header key value in IsatisConfig! ") << ENDL;
    Serial.flush();
    exit(0);
  }

  bool done;
  Serial << ("Header key: 0x");
  Serial.println(EEPROM_KeyValue, HEX);
  unsigned long numRecords=eeprom.getTotalSize()/sizeof(IsatisDataRecord);
  Serial << "EEPROM bank total size: " << eeprom.getTotalSize() << " bytes = " << eeprom.getTotalSize() /1024 << "k" << ENDL;
  Serial << "EEPROM bank can store a total number of " << numRecords << " records." << ENDL;
  numRecords = eeprom.recordsLeftToRead();
  if (numRecords > 0) {
    done = false;
  } else {
    done = true;
    Serial << "EEPROM bank is empty." << ENDL;
  }

  while (!done)
  {
    Serial << "EEPROMS contain " << numRecords << F(" Isatis records") << ENDL;
    while (Serial.available() != 0) {
      Serial.read();  // discard any possible char
    }
    Serial << F("Erase content of EEPROMs ([y]es/[e]xit) ? ");
    while (Serial.available() == 0) {
      ;
    }
    char c = Serial.read();
    while (Serial.available() != 0) {
      Serial.read();  // discard additional chars
    }
    Serial << c << ENDL;
    switch (c)
    {
      case 'y':
        Serial << "Erasing... " << ENDL;
        eeprom.erase();
        numRecords = eeprom.recordsLeftToRead();
        break;
      case 'e':
        done = true;
        break;
      default:
        Serial << "Invalid answer." << ENDL;
    }
  } // while (!done)

  Serial << ENDL;
  Serial << F("End of job") << ENDL;
}

void loop() {

}
